import { Component } from '@angular/core';

import { Serial } from '@ionic-native/serial';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  constructor(private serial: Serial) {}

  serialPortConnect() {
    this.serial.requestPermission().then(() => {
      this.serial.open({
        baudRate: 9800,
        dataBits: 4,
        stopBits: 1,
        parity: 0,
        dtr: true,
        rts: true,
        sleepOnPause: false
      }).then(() => {
        console.log('Serial connection opened');
      });
    }).catch((error: any) => console.log(error));
  }

}
